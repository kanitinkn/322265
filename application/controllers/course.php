<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class course extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->view('_include/header');
        $this->load->view('_include/navbar');
        $this->load->view('course');
        $this->load->view('_include/footer');
    }

}

/* End of file about.php */
/* Location: ./application/controllers/about.php */