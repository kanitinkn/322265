<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class index extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->view('_include/header');
        $this->load->view('_include/navbar');
        $this->load->view('index');
        $this->load->view('_include/footer');
    }

}

/* End of file index.php */
/* Location: ./application/controllers/index.php */