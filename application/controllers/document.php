<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class document extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->view('_include/header');
        $this->load->view('_include/navbar');
        $this->load->view('document');
        $this->load->view('_include/footer');
    }

}

/* End of file document.php */
/* Location: ./application/controllers/document.php */