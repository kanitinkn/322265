<!--<!DOCTYPE html>
<html lang="en">
    <head>
        <title>404 Page Not Found</title>
        <style type="text/css">

            ::selection{ background-color: #E13300; color: white; }
            ::moz-selection{ background-color: #E13300; color: white; }
            ::webkit-selection{ background-color: #E13300; color: white; }

            body {
                background-color: #fff;
                margin: 40px;
                font: 13px/20px normal Helvetica, Arial, sans-serif;
                color: #4F5155;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 19px;
                font-weight: normal;
                margin: 0 0 14px 0;
                padding: 14px 15px 10px 15px;
            }

            code {
                font-family: Consolas, Monaco, Courier New, Courier, monospace;
                font-size: 12px;
                background-color: #f9f9f9;
                border: 1px solid #D0D0D0;
                color: #002166;
                display: block;
                margin: 14px 0 14px 0;
                padding: 12px 10px 12px 10px;
            }

            #container {
                margin: 10px;
                border: 1px solid #D0D0D0;
                -webkit-box-shadow: 0 0 8px #D0D0D0;
            }

            p {
                margin: 12px 15px 12px 15px;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <h1><?php echo $heading; ?></h1>
<?php echo $message; ?>
        </div>
    </body>
</html>-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="DevBear">
        <link rel="shortcut icon" href="assets/img/icon/ballons.ico">

        <title>Error</title>

        <!-- Bootstrap core CSS -->        
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/bootstrap-theme.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="assets/css/custom/googleapis.css" rel="stylesheet">
        <link href="assets/css/pace/red/pace-theme-loading.bar.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet">
        <link href="assets/css/custom/custom.css" rel="stylesheet">
        <link href="assets/css/elements.css" rel="stylesheet">
        <link href="assets/css/custom/font-awesome.min.css" rel="stylesheet">        

    </head>

    <body>

        <!-- header ================================================================ -->
        <!-- Navbar ====Start======================================================= -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">KAEN KAEW</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="hidden-sm hidden-md"><a href="index.php">Home</a></li>
                        <li class="hidden-sm hidden-md"><a href="#">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Navbar ====END========================================================= -->
        <!-- Main body ============================================================= -->
        <div class="not-found">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="digits">
                            4<i class="fa fa-meh-o animated flipInX" id="smile"></i>4
                        </div>
                        <h1>The page you are trying to reach is not found</h1>
                        <h2>You can try to click button home within our website instead:</h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- footer ================================================================ -->
        <footer>
            <div class="container">
                <div class="row">

                    <!-- Contact Us ===============================================  -->
                    <div class="col-sm-6">
                        <div class="headline"><h3>Contact us</h3></div>
                        <div class="content">
                            <p>
                                Phone: 0864604968, 0897108974, 0927580569 (ไก่แก่นแก้ว)<br />
                                Email: <a href="mailto:kanitin.kn@outlook.com">kanitin.kn@outlook.com</a>
                            </p>
                        </div>
                    </div>

                    <!-- Social icons ============================================== -->
                    <div class="col-sm-4">
                        <div class="headline"><h3>Go Social</h3></div>
                        <div class="content social">
                            <ul>
                                <li><a href="inbox.html#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="inbox.html#"><i class="fa fa-facebook"></i></a></li>                        
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>            
                </div>
            </div>
        </footer>

        <!-- Legal ================================================================= -->
        <div class="legal">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p>&copy; kaenkaew.com</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript ============================================= -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/pace/pace.min.js"></script>
        <script src="assets/js/jquery/jquery-2.1.1.min.js"></script>
        <script src="assets/js/bootstrap/bootstrap.min.js"></script>
        <script src="assets/js/isotope/js/isotope-custom.js"></script>
        <script src="assets/js/isotope/js/jquery.isotope.min.js"></script>
        <script src="assets/js/custom/custom.js"></script>
        <script src="assets/js/custom/scrolltopcontrol.js"></script>
    </body>
</html>