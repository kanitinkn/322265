
<!-- Main body ============================================================= -->

<?php title('Documents') ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <!--Row left -->
            <div class="col-md-5 col-md-offset-1">

                <!--Lecture -->
                <div class = "row">
                    <div class = "col-md-12">
                        <h3 class = "hl"><i class="book icon"></i>LECTURE</h3>
                        <table class = "table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center text-muted">
                                    <td>1</td>
                                    <td>LEC#01</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>2</td>
                                    <td>LEC#02</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>3</td>
                                    <td>LEC#03</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>4</td>
                                    <td>LEC#04</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>5</td>
                                    <td>LEC#05</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>6</td>
                                    <td>LEC#06</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>7</td>
                                    <td>LEC#07</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>8</td>
                                    <td>LEC#08</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>

            <!--Row right -->
            <div class="col-md-5">

                <!--Lab -->
                <div class = "row">
                    <div class = "col-md-12">
                        <h3 class = "hl"><i class="lab icon"></i>LAB</h3>
                        <table class = "table table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center text-muted">
                                    <td>1</td>
                                    <td>LAB#01</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>2</td>
                                    <td>LAB#02</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>3</td>
                                    <td>LAB#03</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>4</td>
                                    <td>LAB#04</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>5</td>
                                    <td>LAB#05</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>6</td>
                                    <td>LAB#06</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>7</td>
                                    <td>LAB#07</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>8</td>
                                    <td>LAB#08</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>9</td>
                                    <td>LAB#09</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                                <tr class="text-center text-muted">
                                    <td>10</td>
                                    <td>LAB#10</td>
                                    <td><span class="glyphicon glyphicon-download"></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
