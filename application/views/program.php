
<!-- Main body ============================================================= -->

<?php title('Programs') ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4 col-md-offset-1">
                <img src="<?= base_url() . 'assets/img/program/java.png' ?>" alt="..." class="img-rounded">
            </div>
            <div class="col-md-5 col-md-offset-1">
                <h3 class="hl"><i class="terminal icon"></i>Java SE Development Kit (JDK)</h3>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Program</th>
                            <th class="text-center">Link</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Official Java SE Development Kit (JDK) from Oracle</td>
                            <td class="text-center"><a href="http://www.oracle.com/technetwork/java/javase/downloads/index.html" target="_blank"><span class="glyphicon glyphicon-link"></span></a></td>
                        </tr>
                        <tr>
                            <td>Internal Java SE Development Kit (JDK) for OS X x64</td>
                            <td class="text-muted text-center"><a href=""><span class="glyphicon glyphicon-download"></span></a></td>
                        </tr>
                        <tr>
                            <td>Internal Java SE Development Kit (JDK) for Windows 32 bit</td>
                            <td class="text-muted text-center"><a href=""><span class="glyphicon glyphicon-download"></span></a></td>
                        </tr>
                        <tr>
                            <td>Internal Java SE Development Kit (JDK) for Windows 64 bit</td>
                            <td class="text-muted text-center"><a href=""><span class="glyphicon glyphicon-download"></span></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>        
    </div>
    <div class="ui horizontal icon divider">
        <i class="android icon"></i>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4 col-md-offset-1" style="margin-top: 20px;">
                <img src="<?= base_url() . 'assets/img/program/android.png' ?>" alt="..." class="img-rounded">
            </div>
            <div class="col-md-5 col-md-offset-1">
                <h3 class="hl"><i class="code icon"></i>Android Studio</h3>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Program</th>
                            <th class="text-center">Link</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Official Android Studio from Google</td>
                            <td class="text-center"><a href="http://developer.android.com/sdk/installing/studio.html" target="_blank"><span class="glyphicon glyphicon-link"></span></a></td>
                        </tr>
                        <tr>
                            <td>Internal Android Studio for Mac</td>
                            <td class="text-muted text-center"><a href=""><span class="glyphicon glyphicon-download"></span></a></td>
                        </tr>
                        <tr>
                            <td>Internal Android Studio for Windows</td>
                            <td class="text-muted text-center"><a href=""><span class="glyphicon glyphicon-download"></span></a></td>
                        </tr>                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>        
</div>