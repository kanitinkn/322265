
<!-- Main body ============================================================= -->

<?php title('Course') ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <!--Row left -->
            <div class="col-md-10 col-md-offset-1">

                <!--​Course -->
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="hl"><i class="tasks icon"></i>Course</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Subject</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-muted text-center">1</td>
                                    <td>Install Program & Create Project & Create Emulator</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">2</td>
                                    <td>Design Layout</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">3</td>
                                    <td>Event</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">4</td>
                                    <td>Database with SQLite</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">5</td>
                                    <td>RSS Feed</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">6</td>
                                    <td>Application Android Connect Internet with PHP</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">7</td>
                                    <td>Camera</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">8</td>
                                    <td>Google Map & Basic Function</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">9</td>
                                    <td>Cross Platform</td>
                                </tr>
                                <tr>
                                    <td class="text-muted text-center">10</td>
                                    <td>Test System</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>

            <!--Row right -->
            <div class="col-md-5">

            </div>
        </div>        
    </div>
</div>
