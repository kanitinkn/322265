
<!-- Main body ============================================================= -->

<?php title('Home') ?>

<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-1">           
            <div class="ui piled segment">
                <h2><i class="small red pin basic icon"></i> Say hi...</h2>
                <p class="text-justify">Mobile application development process, the principles of interface design, basic of XML language, program instruction and event trapping, SQLite and database management, RSS Internet Feed, data retrieving by php, camera and photo management, Google Map and the basic functions, cross platform application  development, other interesting functions and applications, system testing.</p>
            </div>
        </div>        
        <div class="col-md-4 col-md-offset-1">

            <!--General Information -->
            <div class = "row">
                <div class = "col-md-12">
                    <h3 class = "hl"><i class="info letter icon"></i> General Information</h3>                        
                    <table class = "table">
                        <tbody>
                            <tr>
                                <td class = "text-muted">Teacher: </td>
                                <td>Dr. SILADA INTASOTHONCHUN</td>
                            </tr>
                            <tr>
                                <td class = "text-muted">Room: </td>
                                <td>SC 6705</td>
                            </tr>
                            <tr>
                                <td class = "text-muted">Email: </td>
                                <td><a href="mailto:silain@kku.ac.th">silain@kku.ac.th</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                </div>
            </div>
        </div>        
        <div class = "row">

            <!--Schedule -->
            <div class = "col-md-6 col-md-offset-1">
                <h3 class = "hl"><i class="calendar icon"></i> Schedule</h3>
                <table class = "table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Section</th>
                            <th>#</th>
                            <th>Day/Time</th>
                            <th>Room</th>
                            <th class="text-center">TA team</th>
                        </tr>
                    </thead>
                    <tbody>

                        <!--Section 1 -->
                        <tr>
                            <td rowspan="2" class="text-muted text-center">1</td>
                            <td>LECTURE</td>
                            <td>Tue 03:00 pm - 05:00 pm</td>
                            <td>SC8505</td>
                            <td rowspan="2" class="text-center">
                                <a href="https://www.facebook.com/kanitin.korngnork" target="_blank"><img src="<?= base_url() . 'assets/img/avatar/sec1_1.png' ?>" class="img-circle"></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://www.facebook.com/peeraphob.chaiyaprom" target="_blank"><img src="<?= base_url() . 'assets/img/avatar/sec1_2.png' ?>" class="img-circle"></a>
                            </td>
                        </tr>
                        <tr>
                            <td>LAB</td>
                            <td>Tue 05:00 pm - 07:00 pm</td>
                            <td>LAB IT</td>
                        </tr>

                        <!--Section 2 -->
                        <tr>
                            <td rowspan="2" class="text-muted text-center">2</td>
                            <td>LECTURE</td>
                            <td>Tue 03:00 pm - 05:00 pm</td>
                            <td>SC8505</td>
                            <td rowspan="2" class="text-center">
                                <a href="https://www.facebook.com/fewsmd" target="_blank"><img src="<?= base_url() . 'assets/img/avatar/sec2_1.png' ?>" class="img-circle"></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://www.facebook.com/I3asta" target="_blank"><img src="<?= base_url() . 'assets/img/avatar/sec2_2.png' ?>" class="img-circle"></a>
                            </td>
                        </tr>
                        <tr>
                            <td>LAB</td>
                            <td>Tue 05:00 pm - 07:00 pm</td>
                            <td>LAB CS</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
            </div><!--./schedole -->

            <div class="col-md-2 col-md-offset-1" style="margin-top: 40px;">                
                <img src="<?= base_url() . 'assets/img/avatar.jpg' ?>" class="img-responsive img-circle">
                <br>
                <div class="ui horizontal icon divider">
                    <i class="heart icon"></i>
                </div>
                <p class="text-center">Big Boss</p>
            </div>
        </div><!--./row -->               
    </div>
</div>
