
<!-- Navbar ====Start======================================================= -->

<div style="margin-top: 40px; margin-bottom: 40px;">
    <div class="container">
        <div class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">322265 MOBILE AND WIRELESS TECHNOLOGY</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?= base_url() . 'index' ?>">HOME</a></li>
                        <li><a href="<?= base_url() . 'course' ?>">COURSE</a></li>
                        <li><a href="<?= base_url() . 'document' ?>">DOCUMENTS</a></li>
                        <li><a href="<?= base_url() . 'program' ?>">PROGRAMS</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="https://www.facebook.com/groups/cs.sj.android/" target="_blank"><i class="facebook icon"></i></a></li>
                        <li><a><i class="github icon"></i></a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </div><!--/.navbar -->
    </div><!--/.container -->
</div>

<!-- Navbar ====END========================================================= -->
