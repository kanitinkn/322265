<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="DevBear">
        <link rel="shortcut icon" href="<?= base_url() . 'assets/img/' ?>">

        <?php

        function title($str) {
            echo "<title>" . $str . "</title>";
        }
        ?>

        <!-- Bootstrap core CSS -->        
        <?= css('bootstrap.css') ?>
        <?= css('bootstrap-theme.min.css') ?>

        <!-- Semantic -->
        <?= css('semantic.min.css') ?>

        <!-- Custom styles for this template -->
        <?= css('custom/googleapis.css') ?>        
        <?= css('pace/red/pace-theme-minimal.css') ?>
        <?= css('animate.css') ?>
        <?= css('custom/custom.css') ?>
        <?= css('elements.css') ?>
        <?= css('custom/font-awesome.min.css') ?>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>

<body style="background-color: #F5F5F5">

<!-- header ================================================================ -->
