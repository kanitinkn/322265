
<!-- footer ================================================================ -->

<div class="footer" style="margin-top: 60px;">
    <div class="container">
        <div class="text-left">
            <p class="text-muted">&copy; Copyright 2012</p>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript ============================================= -->
<!-- Placed at the end of the document so the pages load faster -->
<?= js('pace/pace.min.js') ?>
<?= js('jquery/jquery-2.1.1.min.js') ?>
<?= js('bootstrap/bootstrap.min.js') ?>
<?= js('semantic.min.js') ?>
<?= js('isotope/js/isotope-custom.js') ?>
<?= js('isotope/js/jquery.isotope.min.js') ?>
<?= js('custom/custom.js') ?>
<?= js('custom/scrolltopcontrol.js') ?>
</body>
</html>